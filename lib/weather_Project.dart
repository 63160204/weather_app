import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

const IconData moonStar = IconData(0xf71b, fontFamily: 'MaterialIcons');

void main() {
  runApp(WeatherApp());
}

class MyAppTheme {
  static ThemeData appTheme() {
    return ThemeData(
      brightness: Brightness.light,
      iconTheme: IconThemeData(
        color: Colors.white60,
      ),
    );
  }
}

class WeatherApp extends StatefulWidget {
  @override
  State<WeatherApp> createState() => _WeatherAppState();
}

class _WeatherAppState extends State<WeatherApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: MyAppTheme.appTheme(),
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(60.0),
            child: AppBar(
                flexibleSpace: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(
                            "https://l.facebook.com/l.php?u=https%3A%2F%2Fimages.unsplash.com%2Fphoto-1507400492013-162706c8c05e%3Fixlib%3Drb-4.0.3%26ixid%3DMnwxMjA3fDB8MHxzZWFyY2h8OHx8bmlnaHR8ZW58MHx8MHx8%26w%3D1000%26q%3D80%26fbclid%3DIwAR0lIDaUGjcm-2JWHSMP82LtGw_q4bsGA7MWGXU89IC5QgLmc0LG2ZYCoaw&h=AT3BRqs-6KgabpybwMF4qfED8-Soazs2FOKfByeDcxNAIIVxrLoMFYuhZzIN8zkzQmVIgZcqTWzWezC8DuOiKIa5UsLLnGA5PcRXWM6taHvzfnejIort3uuDTIbGYTq7EGSbxw"),
                        fit: BoxFit.cover,
                      ),
                    )),
                // backgroundColor: Colors.white,
                elevation: 5,
                ),
          ),
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(
                    "https://l.facebook.com/l.php?u=https%3A%2F%2Fimages.unsplash.com%2Fphoto-1507400492013-162706c8c05e%3Fixlib%3Drb-4.0.3%26ixid%3DMnwxMjA3fDB8MHxzZWFyY2h8OHx8bmlnaHR8ZW58MHx8MHx8%26w%3D1000%26q%3D80%26fbclid%3DIwAR0lIDaUGjcm-2JWHSMP82LtGw_q4bsGA7MWGXU89IC5QgLmc0LG2ZYCoaw&h=AT3BRqs-6KgabpybwMF4qfED8-Soazs2FOKfByeDcxNAIIVxrLoMFYuhZzIN8zkzQmVIgZcqTWzWezC8DuOiKIa5UsLLnGA5PcRXWM6taHvzfnejIort3uuDTIbGYTq7EGSbxw"),
                fit: BoxFit.cover,
              ),
            ),
            child: ListView(
              children: <Widget>[
                mainWather(),
                buildDivider(),
                buildDivider(),
                everyday(),
                buildDivider(),
                weatherForecast(),
              ],
            ),
          ),
        ));
  }
}

Widget buildDivider() {
  return const Divider(
    color: Colors.transparent,
  );
}

Widget mainWather() {
  return Column(
    children: <Widget>[
      Text(
        "อ.เมืองชลบุรี",
        style: TextStyle(fontSize: 50, height: 1.3, color: Colors.white),
      ),
      Text(
        "24°",
        style: TextStyle(fontSize: 145, height: 1.3, color: Colors.white),
      ),
      Text(
        "เมฆเป็นบางส่วน",
        style: TextStyle(fontSize: 20, color: Colors.white),
      ),
      Text(
        "สูงสุด:31°  ต่ำสุด:20°",
        style: TextStyle(fontSize: 20, height: 2, color: Colors.white),
      )
    ],
  );
}

Widget everyday() {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.white.withOpacity(0.2),
    child: SizedBox(
      height: 150,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          timeNow(),
          next1hour(),
          next2hour(),
          next3hour(),
          sunrise(),
          next5hour(),
          next6hour(),
          next7hour(),
        ],
      ),
    ),
  );
}

Widget timeNow() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "ตอนนี้",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.moon_stars_fill,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "24°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget next1hour() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "04",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.cloud_moon_fill,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "24°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget next2hour() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "05",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.cloud_moon_fill,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "24°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget next3hour() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "06",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.cloud_moon_fill,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "24°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget sunrise() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "06:39",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.sunrise_fill,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "อาทิตย์ขึ้น",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget next5hour() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "07",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.cloud_sun_fill,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "23°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget next6hour() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "08",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.cloud_sun_fill,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "24°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget next7hour() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "09",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.cloud_sun_fill,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "25°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget weatherForecast() {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.white.withOpacity(0.2),
    child: SizedBox(
      height: 450,
      child: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          Column(
            children: <Widget>[
              wather10Day(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  buildDay(),
                  buildIcon(),
                  buildWeather(),
                ],
              )
            ],
          )
        ],
      ),
    ),
  );
}

Widget wather10Day() {
  return Row(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 20,
          bottom: 20,
          left: 20,
        ),
        child: Text(
          "พยากรณ์อากาศ 7 วัน",
          style: TextStyle(fontSize: 20, color: Colors.white60),
        ),
      ),
    ],
  );
}

Widget buildDay() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "วันนี้",
            style: TextStyle(fontSize: 22, color: Colors.white),
          ),
          Text(
            "พฤ.",
            style: TextStyle(fontSize: 22, height: 2.5, color: Colors.white),
          ),
          Text(
            "ศ.",
            style: TextStyle(fontSize: 22, height: 2.5, color: Colors.white),
          ),
          Text(
            "ส.",
            style: TextStyle(fontSize: 22, height: 2.5, color: Colors.white),
          ),
          Text(
            "อา.",
            style: TextStyle(fontSize: 22, height: 2.5, color: Colors.white),
          ),
          Text(
            "จ.",
            style: TextStyle(fontSize: 22, height: 2.5, color: Colors.white),
          ),
          Text(
            "อ.",
            style: TextStyle(fontSize: 22, height: 2.5, color: Colors.white),
          ),
        ],
      )
    ],
  );
}

Widget buildIcon() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Icon(
            CupertinoIcons.cloud_sun_fill,
            size: 54,
          ),
          Icon(
            CupertinoIcons.cloud_sun_fill,
            size: 54,
          ),
          Icon(
            CupertinoIcons.cloud_fill,
            size: 54,
          ),
          Icon(
            CupertinoIcons.cloud_fill,
            size: 54,
          ),
          Icon(
            CupertinoIcons.cloud_sun_fill,
            size: 54,
          ),
          Icon(
            CupertinoIcons.cloud_sun_fill,
            size: 54,
          ),
          Icon(
            CupertinoIcons.cloud_sun_fill,
            size: 54,
          ),
        ],
      )
    ],
  );
}

Widget buildWeather() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "ต่ำสุด:23°  สูงสุด:31°",
            style: TextStyle(fontSize: 22, color: Colors.white),
          ),
          Text(
            "ต่ำสุด:23°  สูงสุด:32°",
            style: TextStyle(fontSize: 22, height: 2.5, color: Colors.white),
          ),
          Text(
            "ต่ำสุด:23°  สูงสุด:39°",
            style: TextStyle(fontSize: 22, height: 2.5, color: Colors.white),
          ),
          Text(
            "ต่ำสุด:22°  สูงสุด:26°",
            style: TextStyle(fontSize: 22, height: 2.5, color: Colors.white),
          ),
          Text(
            "ต่ำสุด:21°  สูงสุด:29°",
            style: TextStyle(fontSize: 22, height: 2.5, color: Colors.white),
          ),
          Text(
            "ต่ำสุด:21°  สูงสุด:26°",
            style: TextStyle(fontSize: 22, height: 2.5, color: Colors.white),
          ),
          Text(
            "ต่ำสุด:20°  สูงสุด:29°",
            style: TextStyle(fontSize: 22, height: 2.5, color: Colors.white),
          ),
        ],
      )
    ],
  );
}
